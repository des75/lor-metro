// ==UserScript==
// @name        lor-metro
// @namespace   lor-metro
// @description lor-metro
// @include     https://www.linux.org.ru/*
// @include     http://www.linux.org.ru/*
// @version     1
// @grant       none
// ==/UserScript==

/*! Copyright (c) 2013 Brandon Aaron (http://brandon.aaron.sh)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 3.1.9
 *
 * Requires: jQuery 1.2.2+
 */

(function (factory) {
  if ( typeof define === 'function' && define.amd ) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS style for Browserify
    module.exports = factory;
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function ($) {

  var toFix  = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
      toBind = ( 'onwheel' in document || document.documentMode >= 9 ) ?
        ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
      slice  = Array.prototype.slice,
      nullLowestDeltaTimeout, lowestDelta;

  if ( $.event.fixHooks ) {
    for ( var i = toFix.length; i; ) {
      $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
    }
  }

  var special = $.event.special.mousewheel = {
    version: '3.1.9',

    setup: function() {
      if ( this.addEventListener ) {
        for ( var i = toBind.length; i; ) {
          this.addEventListener( toBind[--i], handler, false );
        }
      } else {
        this.onmousewheel = handler;
      }
      // Store the line height and page height for this particular element
      $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
      $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
    },

    teardown: function() {
      if ( this.removeEventListener ) {
        for ( var i = toBind.length; i; ) {
          this.removeEventListener( toBind[--i], handler, false );
        }
      } else {
        this.onmousewheel = null;
      }
    },

    getLineHeight: function(elem) {
      return parseInt($(elem)['offsetParent' in $.fn ? 'offsetParent' : 'parent']().css('fontSize'), 10);
    },

    getPageHeight: function(elem) {
      return $(elem).height();
    },

    settings: {
      adjustOldDeltas: true
    }
  };

  $.fn.extend({
    mousewheel: function(fn) {
      return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
    },

    unmousewheel: function(fn) {
      return this.unbind('mousewheel', fn);
    }
  });

  function handler(event) {
    var orgEvent   = event || window.event,
        args       = slice.call(arguments, 1),
        delta      = 0,
        deltaX     = 0,
        deltaY     = 0,
        absDelta   = 0;
    event = $.event.fix(orgEvent);
    event.type = 'mousewheel';

    // Old school scrollwheel delta
    if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1;      }
    if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;       }
    if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY;      }
    if ( 'wheelDeltaX' in orgEvent ) { deltaX = orgEvent.wheelDeltaX * -1; }

    // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
    if ( 'axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
      deltaX = deltaY * -1;
      deltaY = 0;
    }

    // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
    delta = deltaY === 0 ? deltaX : deltaY;

    // New school wheel delta (wheel event)
    if ( 'deltaY' in orgEvent ) {
      deltaY = orgEvent.deltaY * -1;
      delta  = deltaY;
    }
    if ( 'deltaX' in orgEvent ) {
      deltaX = orgEvent.deltaX;
      if ( deltaY === 0 ) { delta  = deltaX * -1; }
    }

    // No change actually happened, no reason to go any further
    if ( deltaY === 0 && deltaX === 0 ) { return; }

    // Need to convert lines and pages to pixels if we aren't already in pixels
    // There are three delta modes:
    //   * deltaMode 0 is by pixels, nothing to do
    //   * deltaMode 1 is by lines
    //   * deltaMode 2 is by pages
    if ( orgEvent.deltaMode === 1 ) {
      var lineHeight = $.data(this, 'mousewheel-line-height');
      delta  *= lineHeight;
      deltaY *= lineHeight;
      deltaX *= lineHeight;
    } else if ( orgEvent.deltaMode === 2 ) {
      var pageHeight = $.data(this, 'mousewheel-page-height');
      delta  *= pageHeight;
      deltaY *= pageHeight;
      deltaX *= pageHeight;
    }

    // Store lowest absolute delta to normalize the delta values
    absDelta = Math.max( Math.abs(deltaY), Math.abs(deltaX) );

    if ( !lowestDelta || absDelta < lowestDelta ) {
      lowestDelta = absDelta;

      // Adjust older deltas if necessary
      if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
        lowestDelta /= 40;
      }
    }

    // Adjust older deltas if necessary
    if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
      // Divide all the things by 40!
      delta  /= 40;
      deltaX /= 40;
      deltaY /= 40;
    }

    // Get a whole, normalized value for the deltas
    delta  = Math[ delta  >= 1 ? 'floor' : 'ceil' ](delta  / lowestDelta);
    deltaX = Math[ deltaX >= 1 ? 'floor' : 'ceil' ](deltaX / lowestDelta);
    deltaY = Math[ deltaY >= 1 ? 'floor' : 'ceil' ](deltaY / lowestDelta);

    // Add information to the event object
    event.deltaX = deltaX;
    event.deltaY = deltaY;
    event.deltaFactor = lowestDelta;
    // Go ahead and set deltaMode to 0 since we converted to pixels
    // Although this is a little odd since we overwrite the deltaX/Y
    // properties with normalized deltas.
    event.deltaMode = 0;

    // Add event and delta to the front of the arguments
    args.unshift(event, delta, deltaX, deltaY);

    // Clearout lowestDelta after sometime to better
    // handle multiple device types that give different
    // a different lowestDelta
    // Ex: trackpad = 3 and mouse wheel = 120
    if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
    nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

    return ($.event.dispatch || $.event.handle).apply(this, args);
  }

  function nullLowestDelta() {
    lowestDelta = null;
  }

  function shouldAdjustOldDeltas(orgEvent, absDelta) {
    // If this is an older event and the delta is divisable by 120,
    // then we are assuming that the browser is treating this as an
    // older mouse wheel event and that we should divide the deltas
    // by 40 to try and get a more usable deltaFactor.
    // Side note, this actually impacts the reported scroll distance
    // in older browsers and can cause scrolling to be slower than native.
    // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
    return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
  }

}));

//=================================================================================================

(function(document, fn) {
  var script = document.createElement('script');
  script.setAttribute("type", "text/javascript");
  script.textContent = '(' + fn + ')(window, window.document);';
  document.head.appendChild(script); // run the script
  document.head.removeChild(script); // clean up
})(document, function(window, document) {

  (function($){

    $("LINK[href*='combined.css']").remove();

    $(function(){
      function get_comments_count(item){
        var comments_count_string = '';
        var regexp = /&nbsp;комментар(.+)?/

          if(item.is('.news')){
            comments_count_string = $('.nav a', item).html().replace(regexp, "");
          }
        else if(item.is('.mini-news')){
          comments_count_string = $('a:last', item).html().replace(regexp, "");
        }

        return comments_count_string;
      }

      function get_activity_class(count, index){
        count = parseInt(count) || 0;

        var activity_index = Math.floor(count / index) + 1;

        return "activity-" + activity_index;
      }

      function getRandomInt(min, max)
      {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }

      function flip_tile(tile){
        var timeout = getRandomInt(6000, 10000);

        setInterval(function(){
          var tile_b = $('.tile-side:first', tile).remove();

          tile.append(tile_b);
          tile_b.css({
            top: '100%'
          });

          $('.tile-side', tile).animate({
            top: "-=100%"
          }, 400);
        }, timeout);
      }

      function build_boxlets(){
        var boxlets = $('.boxlet').remove();

        $('#boxlets').hide();

        function fill_boxlet_content(boxlet, boxlet_type, boxlet_title){
          var boxlet_links;
          var list_template = $('<div class="metro-column"></div>');

          list_template.append(boxlet_title);

          var rows = Math.floor((parseInt($(window).height()) - 170) / (150 + 10));
          var width_mod = 0;

          if(boxlet_type == 'poll'){
            var item_wrap = $('<div></div>').addClass('activity-1 metro-item metro-item-max');
            var item = $('<div class="tile-side activity-1"></div>');
            item.append($('form', boxlet).clone());
            item_wrap.append(item);
            list_template.append(item_wrap);

            $('a', boxlet).each(function(){
              var item_wrap = $('<div></div>').addClass('activity-1 metro-item metro-item-mini');
              var item = $('<div class="tile-side activity-1"></div>');

              item.html($(this).html());

              var url = $(this).attr('href');
              item.click(function(e){
                if($(e.target).is('a')) return;
                document.location = url;
              });

              item_wrap.append(item);
              list_template.append(item_wrap);
            });
            width_mod = Math.ceil(list_template.children('.metro-item').length / (rows/2));
            $('.metro-item-max, .metro-item-max .tile-side').mousewheel(function(event, delta) {
              this.scrollTop -= (delta * 20);
              event.preventDefault();
            });
          }
          else if(boxlet_type == 'top'){
            $('li', boxlet).each(function(){
              var link = $('a:first', this);
              var activity = $('a:last', this).html();
              var item_wrap = $('<div></div>').addClass(get_activity_class(activity, 1)).addClass('metro-item metro-item-mini');
              var item = $('<div class="tile-side"></div>').addClass(get_activity_class(activity, 1));

              item.html($(link).html());

              var url = $(link).attr('href');
              item.click(function(e){
                if($(e.target).is('a')) return;
                document.location = url;
              });

              item_wrap.append(item);
              list_template.append(item_wrap);
            });
            width_mod = Math.ceil(list_template.children('.metro-item').length / rows);
          }
          else if(boxlet_type == 'tag_cloud'){
            $('a', boxlet).each(function(){
              var link = $(this);
              var classname = link.attr('class');
              var activity;
              if(classname){
                activity = classname.replace("cloud", "");
              }
              else{
                activity = 0;
              }
              var item_wrap = $('<div></div>').addClass(get_activity_class(activity, 1)).addClass('metro-item metro-item-tiny');
              var item = $('<div class="tile-side"></div>').addClass(get_activity_class(activity, 1));

              item.html($(link).html());

              var url = $(link).attr('href');
              item.click(function(e){
                if($(e.target).is('a')) return;
                document.location = url;
              });

              item_wrap.append(item);
              list_template.append(item_wrap);
            });
            width_mod = Math.ceil(list_template.children('.metro-item').length / (rows *4));
          }
          else if(boxlet_type == 'archive'){
            $('a', boxlet).each(function(){
              var link = $(this);
              var activity = link.html().match(/(\d+)/ig);

              if(activity){
                activity = activity[1];
              }
              else{
                activity = 0;
              }

              var item_wrap = $('<div></div>').addClass(get_activity_class(activity, 20)).addClass('metro-item metro-item-mini');
              var item = $('<div class="tile-side"></div>').addClass(get_activity_class(activity, 20));

              item.html($(link).html());

              var url = $(link).attr('href');
              item.click(function(e){
                if($(e.target).is('a')) return;
                document.location = url;
              });

              item_wrap.append(item);
              list_template.append(item_wrap);
            });
            width_mod = Math.ceil(list_template.children('.metro-item').length / rows);
          }
          else{
            $('a', boxlet).each(function(){
              var item_wrap = $('<div></div>').addClass('activity-1 metro-item metro-item-mini');
              var item = $('<div class="tile-side activity-1"></div>');

              item.html($(this).html());

              var url = $(this).attr('href');
              item.click(function(e){
                if($(e.target).is('a')) return;
                document.location = url;
              });

              item_wrap.append(item);
              list_template.append(item_wrap);
            });
            width_mod = Math.ceil(list_template.children('.metro-item').length / rows);
          }

          list_template.addClass('width-'+width_mod);

          /*

           var rows = Math.floor((parseInt($(window).height()) - 170) / (150 + 10));
           width_mod = Math.ceil(boxlet_links.length / rows);  // 4 строки плиток

           list_template.addClass('width-'+width_mod);

           boxlet_links.each(function(){
           var item_wrap = $('<div></div>').addClass('metro-item metro-item-mini activity-1');
           var item = $('<div class="tile-side activity-1"></div>');
           item.html($(this).html());
           var url = $(this).attr('href');

           item.click(function(e){
           if($(e.target).is('a')) return;
           document.location = url;
           });

           item_wrap.append(item);

           list_template.append(item_wrap);
           });*/

          $('.metro-column:last').after(list_template);
        }

        boxlets.each(function(){
          var boxlet_title = $('h2', this).clone();
          var boxlet_title_text = boxlet_title.html();
          var boxlet_type = false;

          var boxlet_type_relation = {
            'gallery' : /(.+)?Галерея(.+)?/ig,
            'poll' : /(.+)?Опрос(.+)?/ig,
            'top' : /(.+)?Top(.+)?/ig,
            'tag_cloud' : /(.+)?Облако\ Меток(.+)?/ig,
            'archive' : /(.+)?Архив(.+)?/ig
          }

          for(var t in boxlet_type_relation){
            if(boxlet_type_relation[t].test(boxlet_title_text)){
              boxlet_type = t;
              break;
            }
          }

          switch (boxlet_type){
          case 'gallery' : return;
            default : fill_boxlet_content($('.boxlet_content', this), boxlet_type, boxlet_title);
          }

        });
      }

      function build_metro_tiles(news, filter_months){
        //var news = $('.news, .mini-news');
        var current_date_marker = 0;
        var all_news_collection = [];
        var by_month_collection = [];
        var has_mini = false;
        var post_date_string = 1;

        news.each(function(){
          var item = $(this);
          var post_timestamp = $('time', item);
          var post_date = 0;
          var url = "";

          post_date_string = 1;

          var comments_count = get_comments_count(item);

          var tmpclass = item.attr('class');
          item.attr('class', get_activity_class(comments_count, 50));
          item.addClass(tmpclass);

          item.wrapInner('<div class="tile-side"></div>');
          if($('.medium-image', item).length > 0){
            // скриншот, вытаскиваем картинку
            var screenshot_link_with_image = $('img.medium-image', item).clone();
            screenshot_link_with_image.on('load', function(){
              screenshot_link_with_image.css({
                marginTop: -((parseInt(screenshot_link_with_image.height()) - 150) / 2) + 'px',
              });
              flip_tile(item);
              // | если надо чтоб сначала показывался скриншот, поменять на appendTo.
            });                                                                // v бывают подергивания, поэтому лучше оставить так
            $('<div class="tile-side"></div>').append(screenshot_link_with_image).prependTo(item);
          }

          $('.tile-side', item).addClass(get_activity_class(comments_count, 50));

          if(item.is('.mini-news')){
            var html = item.html();  // |
            has_mini = true;         // v убираем список страниц мини-новости вместе с квадратными скобками
            html = html.replace("\[<a", "<div class='nav'><a").replace("a>\]", "a></div>").replace("\)\]", "\)</div>");
            item.html(html);
            url = $('a:first', item).attr('href');
          }
          else{
            var header = $('h2 a', item).html();
            url = $('h2 a', item).attr('href');

            if(header.length > 80){
              $('h2 a', item).html(header.substr(0, 80) + "...");
            }
          }

          item.click(function(e){
            if($(e.target).is('a')) return;

            document.location = url;
          });

          if(post_timestamp.length > 0){
            post_date = post_timestamp.attr('datetime');
            post_date = new Date(post_date);
            post_date_string = '' + post_date.getFullYear() + '-' + post_date.getMonth();

            if(typeof filter_months == 'undefined' || !filter_months){
              post_date_string += '-' + post_date.getDate();
            }
          }

          if(current_date_marker != post_date_string && post_date){

            if(!current_date_marker){
              by_month_collection.push(this);
            }
            else{
              all_news_collection.push(by_month_collection);
              by_month_collection = [this];
            }

            current_date_marker = post_date_string;
          }
          else{
            by_month_collection.push(this);
          }

        });
        all_news_collection.push(by_month_collection);

        for(var i in all_news_collection){
          var wrapper = $("<div class='metro-column'></div>");
          var width_mod = 6;
          var mini = 0;

          var rows = Math.floor((parseInt($(window).height()) - 170) / (150 + 10));

          width_mod = Math.ceil(all_news_collection[i].length / rows);  // 4 строки плиток

          for (var min in all_news_collection[i]){
            if($(all_news_collection[i][min]).is('.mini-news')){
              mini++;
            }
          }

          if(mini){
            width_mod = width_mod * 2 + Math.floor(mini / 3); // magic numbers
          }                                                   // непонятно как считать ширину блока если есть мини-плитки
          // то что есть сделано путем подбора
          else{
            width_mod *= 2; // тут все нормально, тк блоки одинаковые
          }

          wrapper.addClass('width-'+width_mod);
          $(all_news_collection[i]).wrapAll(wrapper);
        }

        build_boxlets();

        var summaryWidth = 0;
        $('.metro-column').each(function(){
          summaryWidth += $(this).outerWidth();
        });

        $('body').css({
          'height' : '100%',
          'position' : 'absolute'
        });

        $('#bd').css({
          marginTop: '50px',
          width: summaryWidth  + 260 + 'px' // 200px css padding, 60px ширина маленького блока на всякий случай
        });

        $('html, body').mousewheel(function(event, delta) {
          this.scrollLeft -= (delta * 100);
          event.preventDefault();
        });
      }

      function build_metro_buttons(){
        var links = $('#bd .nav a:not(article .nav a, .page-number a), .message-table tfoot a, [rel="prev"], [rel="next"]');
        var page_numbers = $('#comments > .nav .page-number');
        var new_link;
        var link_name;

        var links_parse = {
          back: [/(.+)?←(.*)?/i, /(.+)?предыдущ(.+)?/i, /(.+)?назад(.+)?/i],
          forward: [/(.+)?→(.+)?/i, /(.+)?следующ(.+)?/i, /(.+)?вперед(.+)?/i],
          add: [/(.+)?Добав(.+)?/i],
          archive: [/(.+)?Архив(.+)?/i],
          temp: [/(.+)?Неподтв(.+)?/i]
        };

        var footer_toolbar = $('<div id="footer-toolbar"></div>');
        footer_toolbar['opened'] = false;

        $('body').append(footer_toolbar);

        var pages = {};

        links.each(function(){
          link_name = $(this).html();

          new_link = $('<a></a>').addClass('metro-button').attr({
            href: $(this).attr('href'),
            title: link_name
          });

          for(var classname in links_parse){
            var cls = links_parse[classname];
            var br = false;
            for(var regex in cls){
              if(cls[regex].test(link_name)){
                new_link.addClass(classname);
                footer_toolbar.append(new_link);
                br = true;
                delete links_parse[classname];
                break;
              }
            }
            if(br) break;
          }
        });

        var pagenum_parse = {
          back: /(.+)?←(.*)?/i,
          forward: /(.+)?→(.+)?/i
        };
        page_numbers.each(function(){
          link_name = $(this).html();
          new_link = $(this).clone().addClass("metro-button");

          for(var pt in pagenum_parse){
            if(pagenum_parse[pt].test(link_name)){
              new_link.addClass(pt).empty();
              break;
            }
          }

          footer_toolbar.prepend(new_link);
        });

        footer_toolbar.wrapInner('<div class="mb-wrapper"></div>');
        var metro_buttons_count = $('.metro-button', footer_toolbar).length;
        if((parseInt($('.mb-wrapper', footer_toolbar).width()) / metro_buttons_count) < 62){
          $('.mb-wrapper', footer_toolbar).css({
            width: (metro_buttons_count + 1) * 62 + 200 + 'px'

          });
        }

        footer_toolbar.mousewheel(function(event, delta) {
          this.scrollLeft -= (delta * 62);
          event.preventDefault();
        });

        function bind_toolbar_showing(){
          //var timer;

          function hide_toolbar(){
            footer_toolbar.animate({marginTop: "-2px"}, 200, function(){
              footer_toolbar.opened = false;
              bind_toolbar_showing();
              //  clearTimeout(timer);
            });
          }

          footer_toolbar.one('mouseover', function(){
            if(!footer_toolbar.opened){
              footer_toolbar.animate({marginTop: "-70px"}, 200, function(){
                footer_toolbar.opened = true;

                //timer = setTimeout(hide_toolbar, 5000);

                $(window).on('click', function hide_footer_toolbar_by_click(e){
                  if(!$(e.target).is(footer_toolbar)){
                    hide_toolbar();
                    $(window).off('click', hide_footer_toolbar_by_click);
                  }
                });

              });
            }
          });
        }

        bind_toolbar_showing();
      }

      function build_metro_chat(items){
        var op_message = $('.messages > article.msg');
        var last_breadcrumb = $('.msg-top-header a:last', op_message).clone();

        var top_button_back = $('<a></a>').addClass('metro-button back').attr({
          href: last_breadcrumb.attr('href'),
          title: $('.msg-top-header',op_message).html().replace(/<[^>]*>/ig, "").replace(/\s+/ig, "")
        });

        $('#hd').prepend(top_button_back);

        if($('figure.medium-image', op_message).length > 0){
          $('div[itemprop="articleBody"]', op_message).prepend($('figure.medium-image', op_message).remove());
        }

        items.each(function(){
          var item = $(this);
          var message_controls = [];

          var answer_to = $('.title a', item);

          // контролы сообщения, сделано совершенно тупо, но работает
          if(answer_to.length > 0){
            var newlink = answer_to.clone().empty();
            message_controls.push(
              newlink.addClass('metro-button answer_to').attr({
                href: answer_to.attr('href'),
                title: $('.title', item).html().replace(/<[^>]*>/ig, "")
              })
            );
          }
          if(localStorage){
            var user = $('[itemprop="creator"]', item).html();
            var avatar_src = localStorage.getItem(user);

            if(!avatar_src || typeof avatar_src == 'undefined'){
              avatar_src   = $('.userpic .photo', item).attr('src');
              localStorage.setItem(user, avatar_src);
            }
          }

          $('.reply a', item).each(function(){
            var lnk = $(this).clone().empty();
            var buff = lnk.attr('href').match(/\d+/g);
            var idr = buff[1];

            if(lnk.is('a[href^="add_comment.jsp"]')){
              lnk.bind("click", function() {
                sh(1, idr);
                return false;
              }).addClass('metro-button reply_to');
            }
            else if(lnk.is('a[href^="comment-message.jsp"]')){
              lnk.bind("click", function() {
                sh(0,0);
                return false;
              }).addClass('metro-button reply_to');
            }
            else if(lnk.is('a[href^="edit.jsp"]') || lnk.is('a[href^="/edit_comment"]')){
              lnk.addClass('metro-button edit');
            }
            else if(lnk.is('a[href^="/delete_comment.jsp"]')){
              lnk.addClass('metro-button delete');
            }
            else{
              lnk.addClass('metro-button link');
            }
            message_controls.push( lnk );
          });

          var title = $('.title', item);
          var regex_deleted = /(.+)?удалено(.+)?/i;

          if(regex_deleted.test(title.html())){
            $('.msg_body', item).addClass('deleted');
          }

          if(item.is(op_message)){
            $('.msg_body div[itemprop="articleBody"]', item).prepend(message_controls.reverse());
          }
          else{
            $('.msg_body', item).prepend(message_controls.reverse());
          }
        });

        var poll_variants = $('.poll-result li');
        if(poll_variants.length > 0){
          poll_variants.each(function(){
            var pv_percent = $('.penguin_percent', this).html().match(/(\d+)%/gi)[0];
            $('.penguin_progress > span', this).css({
              width: pv_percent
            });

            var activity = get_activity_class(parseInt(pv_percent), 10);

            $('.penguin_progress', this).addClass(activity);
          });
        }

      }

      function build_main_menu(){
        var lists = $('#bd ul');
        var headers = ['Форум', 'RSS подписки', 'Настройки'];

        var output = [];
        var icons = {                    // заменить буквы на классы
          'General' : 'a',
          'Desktop' : 'b',
          'Admin' : 'c',
          'Linux-install' : 'd',
          'Development' : 'e',
          'Linux-org-ru' : 'f',
          'Security' : 'h',
          'Linux-hardware' : 'i',
          'Talks' : 'j',
          'Job' : 'k',
          'Games' : 'l',
          'Web-development' : 'm',
          'Клуб' : 'n',
          'Lor-source' : 'o',
          'Mobile' : 'p',
          'Multimedia': 'q',
          'Midnight Commander' : 'r'
        };

        lists.each(function(){
          var list = $('li', this);
          var metro_list = [];

          list.each(function(){
            var item = $('<div></div>');
            var today_posted = $(this).html().match(/сегодня\ (\d+)/gi);
            var today_posts;
            var ic = '';
            var url = $('a', this).attr('href');

            if(today_posted){
              today_posts = today_posted[0].replace("сегодня ", "");
              ic = icons[$('a', this).html()];
            }
            else{
              today_posts = 0;
            }
            item.addClass(get_activity_class(today_posts, 100)).addClass('metro-item');

            var wrapper = $("<div></div>").addClass(get_activity_class(today_posts, 100)).addClass('tile-side');
            item.html("<span class='fontello'>"+ic+"</span><div class='entry-body'>"+$('a', this).html()+"</div>");
            item.wrapInner(wrapper);

            item.click(function(e){
              if($(e.target).is('a')) return;

              document.location = url;
            });

            metro_list.push(item);
          });

          //$(metro_list).wrapAll("<div class='metro-column'></div>");
          output.push(metro_list);
        });

        $('#bd').empty();

        var rows = Math.floor((parseInt($(window).height()) - 170) / (150 + 10));

        var x = 0;
        var widths = [(Math.ceil(17/rows)*2),(Math.ceil(3/rows)*2),(Math.ceil(4/rows)*2)];   // список форумов не меняется, так что пусть так
        for(var i in output){
          var wr = $("<div class='metro-column'></div>");
          wr.append(output[i]).addClass("width-"+widths[x]);
          x++;

          $('#bd').append(wr);
        }

        var summaryWidth = 0;
        $('.metro-column').each(function(){
          summaryWidth += $(this).outerWidth();
        });

        $('body').css({
          'height' : '100%',
          'position' : 'absolute'
        });

        $('#bd').css({
          marginTop: '50px',
          width: summaryWidth  + 200 + 'px' // 200px боковой padding #bd в css
        });

        $('html, body').mousewheel(function(event, delta) {
          this.scrollLeft -= (delta * 100);
          event.preventDefault();
        });
      }

      function build_metro_forum(main_column, where_posts_count){
        main_column = parseInt(main_column);
        $('.forum table').hide();
        var list_template = $('<div class="metro-list"></div>');
        var list = list_template.clone();
        var threads = $('.forum table tbody tr');
        var counter = 0;
        var threads_in_column = Math.ceil(threads.length / 2);

        threads.each(function(){
          var list_item = $('<div class="metro-list-item"></div>');

          var tags_array = [];

          var title = $('<h3></h3>');
          var tags = $('<span class="tags icon-tag"></span>');
          var tile = $('<div class="tile"></div>');

          var column = $('td:eq('+main_column+')', this);

          if(column.children(':first').is('img')){
            var img_alt = column.children(':first').attr('alt');

            switch(img_alt){
            case 'Прикреплено' : tile.addClass('icon-pin'); break;
            case 'Решено' : tile.addClass('icon-ok'); break;
            case '[X]' : tile.addClass('icon-cancel'); break;
            }
          }

          function resize_avatars(){
            var image = $(this);
            tile.addClass('with-avatar');
            tile.append(image);

            var W = parseInt(image.width());
            var H = parseInt(image.height());

            if(W > H){
              var mod = 50 / H;
              image.css({
                height: '100%',
                marginLeft: -((W*mod - 50) / 2) + 'px',
              });
            }else{
              var mod = 50 / W;
              image.css({
                width: '100%',
                marginTop: -((H*mod - 50) / 2) + 'px',
              });
            }
          };

          var title_link = $('a:first', column).clone();

          if(localStorage){
            $('*', column).remove();

            var user = column.html().match(/\(([^()]*)\)/ig);

            if(user){
              user = user[user.length - 1].replace(/[\(\)]/ig, "");

              var src = localStorage.getItem(user);

              if(!src || typeof src == 'undefined'){
                if(user && user != '') {
                  $.get("/people/"+user+"/profile", function(content){
                    src = $('.userpic .photo', content).attr('src');
                    localStorage.setItem(user, src);
                    if(src && src != ''){
                      var img = new Image();
                      img.src = src;
                      img.onload = resize_avatars;
                    }
                  });
                }
              }else{
                var img = new Image();
                img.src = src;
                img.onload = resize_avatars;
              }
            }

          }

          var url = title_link.attr("href");

          $('.tag', title_link).each(function(){
            tags_array.push($(this).html());
            $(this).remove();
          });

          list_item.click(function(e){
            if($(e.target).is('a')) return;

            document.location = url;
          });

          title.append(title_link);

          var today_posts = 0;
          if(where_posts_count){
            today_posts =  parseInt($(where_posts_count, this).html());
          }
          list_item.addClass(get_activity_class(today_posts, 50));

          list_item.append(tile).append(title);

          if(tags_array.length > 0){
            tags.html(tags_array.join(", "));
            list_item.append(tags);
          }

          list.append(list_item);

          counter++;

          if(counter >= threads_in_column){
            $('.forum').append(list);
            list = list_template.clone();
            counter = 0;
          }

        });

        $('.forum').append(list);

      }

      function update_menu_activity(){
        var where = document.location.pathname;
        var menu = $('#hd li a');
        var relation = {
          "/news/" : [/\/news(.+)?/i, /^\/$/],
          "/gallery/" : [/\/gallery(.+)?/i],
          "/forum/" : [/\/forum(.+)?/i, /\/people(.+)?/, /\/show-comments(.+)?/, /\/polls(.+)?/],
          "/tracker/" : [/\/tracker(.+)?/i],
          "/wiki/" : [/\/wiki(.+)?/i],
          "notifications" : [/\/notifications(.+)?/i],
          "/search.jsp" : [/\/search(.+)?/i]
        }

        for(var url in relation){
          var re = relation[url];
          var br = false;
          for(var regex in re){
            if(re[regex].test(where)){
              menu.filter('[href="'+url+'"]').parent().addClass('active');
              br = true;
              break;
            }
          }
          if(br) break;
        }
      }

      function build_metro_sidebar(){
        var sidebar = $('<div id="metro-sidebar"></div>');

        var forms = $('#bd > form:not(#profileForm, #editRegForm, #query, #messageForm)');

        if(forms.length > 0){
          sidebar.append(forms.remove());

          $('body').append(sidebar);

          function bind_sidebar_showing(){
            //var timer;

            function hide_sidebar(){
              sidebar.animate({marginLeft: "-3px"}, 500, function(){
                sidebar.opened = false;
                bind_sidebar_showing();
                //clearTimeout(timer);   // боковую панель по таймеру не прячем
              });
            }

            sidebar.one('mouseover', function(){
              if(!sidebar.opened){
                sidebar.animate({marginLeft: "-400px"}, 500, function(){
                  sidebar.opened = true;

                  //timer = setTimeout(hide_sidebar, 5000);

                  $(window).on('click', function hide_metro_sidebar_by_click(e){
                    if(!$(e.target).is(sidebar) && $(e.target).parents('#metro-sidebar').length == 0){
                      hide_sidebar();
                      $(window).off('click', hide_metro_sidebar_by_click);
                    }
                  });

                });
              }
            });
          }

          bind_sidebar_showing();
        }
      }

      $('.news, .mini-news').wrapAll("<div class='metro-part-wrapper'></div>");

      if($('#news .news, .mini-news').length > 0){
        var path = document.location.pathname;

        switch(path){
          default: build_metro_tiles($('.news, .mini-news'), false);
        }
      }
      else if($('.messages .msg').length > 0){
        build_metro_chat($('.msg'));                // страница темы с сообщениями
      }
      else if($('.forum table').length > 0){
        var path = document.location.pathname;
        // таблица форума, 2 колонки шириной 900пх
        switch(path){

        case '/tracker/' : build_metro_forum(1, 'td.numbers'); break; // в трекере смотрим 2 столбец, а не 1-й
        case '/notifications': build_metro_forum(1, false); break;
        case '/show-comments.jsp': build_metro_forum(2, false); break;
          // :first - сообщений в теме, :eq(1) - за день, :eq(2) за час
          // v
        default: build_metro_forum(0, 'td.numbers b:eq(1)'); break;
        }
      }
      else{                                              // разные специфические варианты
        var path = document.location.pathname;

        switch(path){
        case '/forum/' : build_main_menu(); break;    // например список форумов - похож на 1 вариант, но с уникальными иконками
        case '/gallery/' : build_metro_tiles($('.news, .mini-news'), false); break; // ксотыль
        case '/polls/' : build_metro_tiles($('.news, .mini-news'), true); break;
        }
      }
      //build_boxlets();
      build_metro_buttons();                      // нижняя панель с остатками блоков .nav
      build_metro_sidebar();
      update_menu_activity();                      // меню в шапке, ставим выбранный пункт

      // в stylish какойто глюк, не применяются стили элементов форм
      // вылечил так:
      $('[type=submit],[type=button], select, button').addClass('metro-input-button').css(
        "background-color", "black"
      );

      $('[type=text],[type=password], [type=email], [type=number], textarea').addClass('metro-input').css(
        "background-color", "white"
      );
    });

  })(jQuery);

});
